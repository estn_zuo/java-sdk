/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.zuoliubo.common.http;

import com.zuoliubo.common.pojo.Result;
import com.zuoliubo.common.util.MD5Util;
import com.zuoliubo.common.sign.SignSupport;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Description: DemoTest
 * @Author: estn.zuo
 * @CreateTime: 2017-03-21 11:19
 */
public class DemoTest {

    private static final String APPID = "b3ef92e1b5a24a5da2625e62ccb85929";
    private static final String KEY = "91c490ee1429cead";
    private static final String URL = "https://www.91xrj.com/xrj-api/v1/";


    @Test
    public void testDemo() {
        Map<String, Object> param = new HashMap();
        param.put("stamp", UUID.randomUUID().toString());
        param.put("appid", APPID);
        param.put("args", "abcd");
        String newSignString = SignSupport.createLinkString(param) + "&" + KEY;
        param.put("sign", MD5Util.encrypt(newSignString));
        Result result = HttpUtil.post(URL + "/demo", param);
        System.out.println(result);


    }
}
