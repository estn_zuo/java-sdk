/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.zuoliubo.common.http;

import com.zuoliubo.common.util.MD5Util;
import com.zuoliubo.common.sign.SignSupport;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Description: DemoUpload
 * @Author: estn.zuo
 * @CreateTime: 2017-03-21 11:21
 */
public class DemoUpload {

    private static final String APPID = "b3ef92e1b5a24a5da2625e62ccb85929";
    private static final String KEY = "91c490ee1429cead";
    private static final String URL = "https://www.91xrj.com/xrj-api/v1/";

    @Test
    public void testUpload() throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("appid", APPID);
        param.put("stamp", UUID.randomUUID().toString());

        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost httppost = new HttpPost(URL + "resources");
        FileBody userAvatar = new FileBody(new File(new String("/Users/estn/Desktop/1.png".getBytes())), "image/*");
        FileBody userAvatar1 = new FileBody(new File(new String("/Users/estn/Desktop/1.png".getBytes())), "image/*");
        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, null, Charset.forName("UTF-8"));
        String newSignString = SignSupport.createLinkString(param) + "&" + KEY;
        System.out.println(newSignString);
        reqEntity.addPart("sign", new StringBody(MD5Util.encrypt(newSignString)));
        for (Map.Entry entry : param.entrySet()) {
            reqEntity.addPart(entry.getKey().toString(), new StringBody(entry.getValue().toString(), Charset.forName("utf-8")));
        }
        reqEntity.addPart("file", userAvatar);
        reqEntity.addPart("file", userAvatar1);

        httppost.setEntity(reqEntity);

        HttpResponse response = httpclient.execute(httppost);
        HttpEntity resEntity = response.getEntity();
        System.out.println(EntityUtils.toString(resEntity));
    }
}
