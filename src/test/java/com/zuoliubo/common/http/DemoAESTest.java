/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.zuoliubo.common.http;

import com.zuoliubo.common.pojo.Result;
import com.zuoliubo.common.util.AESUtil;
import com.zuoliubo.common.util.EncryptException;
import com.zuoliubo.common.util.MD5Util;
import com.zuoliubo.common.sign.SignSupport;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Description: DemoAESTest
 * @Author: estn.zuo
 * @CreateTime: 2017-03-21 11:23
 */
public class DemoAESTest {

    private static final String APPID = "b3ef92e1b5a24a5da2625e62ccb85929";
    private static final String KEY = "91c490ee1429cead";
    private static final String URL = "https://www.91xrj.com/xrj-api/v1/";

    @Test
    public void testAES() throws EncryptException {
        String password = "123456";
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("appid", APPID);
        param.put("username", "18010558730");
        param.put("stamp", UUID.randomUUID().toString());
        param.put("password", AESUtil.encrypt(password, KEY));
        param.put("validateCode", "754776");
        param.put("nickname", "不错哟");
        param.put("gender", "MALE");
        param.put("birthday", "1987-02-10");
        param.put("email", "5696641@qq.com");
        param.put("recommendUsername", "1");
        param.put("avatar", "/resources/201603/21/0ef9d23f10c04a48b637666b7e22a1b2.png");

        String newSignString = SignSupport.createLinkString(param);
        newSignString = newSignString + "&" + KEY;
        param.put("sign", MD5Util.encrypt(newSignString));
        Result result = HttpUtil.post(URL + "auth/register", param);

        System.out.println(result);
    }
}
