package com.zuoliubo.common.util;

public class EncryptException extends Exception {

    private static final long serialVersionUID = 1572699429277957109L;

    public EncryptException() {
    }

    public EncryptException(String message) {
        super(message);
    }

    public EncryptException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncryptException(Throwable cause) {
        super(cause);
    }
}